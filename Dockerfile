FROM node:10

WORKDIR /usr/src/app/bbva-hackathon-front

COPY package*.json ./

RUN npm install -g @angular/cli @angular-devkit/build-angular && npm install

EXPOSE 4200

CMD ["npm", "start"]