const express = require('express');
const app = express(),
      bodyParser = require("body-parser");
      port = 3080;

const users = [];

app.use(bodyParser.json());
app.use(express.static(process.cwd()+"/my-app/dist/angular-nodejs-docker-compose/"));

app.get('/api/users', (req, res) => {
  res.json(users);
});

app.get('/', (req,res) => {
  res.json("Api v1.1");
});

app.listen(port, () => {
    console.log(`Server listening on the port::${port}`);
});
