import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';  
import { LayoutsRoutingModule } from './layout-routing.module';
import { LoginComponent } from '../components/login/login.component';
import { RegisterComponent } from '../components/register/register.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { ExternalLayoutComponent } from './external-layout/external-layout.component';
import { HeaderComponent } from '../components/header/header.component';
import { FooterComponent } from '../components/footer/footer.component';
import { DashboardComponent } from '../components/main/dashboard/dashboard.component';
import { DashboardClientComponent } from '../components/main/dashboard-client/dashboard-client.component';
import { CarouselComponent } from '../components/main/carousel/carousel.component';

const COMPONENTS = [
  MainLayoutComponent,
  ExternalLayoutComponent,
  LoginComponent,
  RegisterComponent,
  HeaderComponent,
  FooterComponent,
  DashboardComponent,
  DashboardClientComponent,
  CarouselComponent,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutsRoutingModule
  ],
  declarations: [...COMPONENTS]
})
export class LayoutsModule {}
