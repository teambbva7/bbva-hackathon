import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExternalLayoutComponent } from './external-layout/external-layout.component';
import { LoginComponent } from '../components/login/login.component';
import { RegisterComponent } from '../components/register/register.component';
import { DashboardComponent } from '../components/main/dashboard/dashboard.component';

import { AuthGuard } from '../core/auth.guard';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { DashboardClientComponent } from '../components/main/dashboard-client/dashboard-client.component';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard-client',
        component: DashboardClientComponent
      },
      {
        path: 'client-acount',
        loadChildren: () => import('../components/cuenta/cuenta.module').then(m => m.CuentaModule)
      },
    ]
  },
  {
    path: '',
    component: ExternalLayoutComponent,
    children: [
      { path: 'dashboard', canActivate: [AuthGuard], component: DashboardComponent },
      { path: 'login', canActivate: [AuthGuard], component: LoginComponent },
      { path: 'register', component: RegisterComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutsRoutingModule {}
