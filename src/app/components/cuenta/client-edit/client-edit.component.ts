import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router, UrlSegment } from '@angular/router';

@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.css']
})
export class ClientEditComponent implements OnInit {

  public clientObject: any = {};
  public id: string = '';

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.checkObservable();
  }

  checkObservable() {
    this.activatedRoute.url.subscribe((url: UrlSegment[]) => {
      // get cliente
    });
  }

  save() {
    this.router.navigate(['dashboard-client', 'edit', this.id]);
  }

  back() {
    this.router.navigate(['dashboard-client', this.id]);
  }

}
