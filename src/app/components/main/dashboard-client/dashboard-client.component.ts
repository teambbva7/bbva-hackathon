import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-client',
  templateUrl: './dashboard-client.component.html',
  styleUrls: ['./dashboard-client.component.css']
})
export class DashboardClientComponent implements OnInit {

  constructor(
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  editClient() {
    this.router.navigate(['client-acount', 'edit']);
  }

  editAcount() {
    this.router.navigate(['client-acount', 'acount']);
  }
}
